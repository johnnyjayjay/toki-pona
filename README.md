# lipu suwi pi toki pona

This repository contains the [slides](./slides.md) and the (rough) [script](./notes.md) for a small (6 minutes) presentation I gave about the constructed language [toki pona](https://tokipona.org/).

The slides are live on [Codeberg pages](https://johnnyjayjay.codeberg.page/toki-pona/).

Made with [slidev](https://sli.dev).

## Use locally

To start the slide show locally:

- `npm install`
- `npm run dev`
- visit http://localhost:3030

Edit the [slides.md](./slides.md) to see the changes.

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

