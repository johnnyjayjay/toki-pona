
## Intro
- There is a hypothesis in linguistics, a conjecture, that says:
  The language we speak influences the way we think and even the way we perceive reality
- this is what's known as the hyptothesis of linguistic relativity, and there is empirical evidence that supports it.
- an example: if you speak a language that has less words and grammar than another language, such that you are forced to communicate in simpler terms, you will focus more on the core of what you want to say and what's really important rather than details.
- Today, I would like to take you on a small journey and show you a language that takes this idea to an extreme. A language that is so simple that you can learn it in less than a day.
- I am talking about toki pona.

## Conlangs
- the first thing I should mention is that toki pona is a constructed language,
- a constructed language (conlang for short), as opposed to a natural language, does not come from natural development, but intentional design by a person or organisation
- different types of conlangs
  - international auxiliary languages try to be global languages that can be spoken by everyone (you might have heard of Esperanto)
  - fictional languages are made up for Science-Fiction, Fantasy (Klingon, Sandarin)
  - artistic languages (linguistic experiments and explorations) - this is what toki pona is

## Phonology
- phonology is the way a language sounds (vowels and consonants and how they can be combined)
- fun question: guess how many vowels the French language has
  - 14
  - consonants also complicated
  - most natural language have a complicated phonetic inventory, can't have that for toki pona
- toki pona has 9 consonants and 5 vowels
- we pronounce words exactly as they're written: vowels are like in Spanish or Japanese, consonants are pronounced like in English (except j)
- this inventory happens to be compatible with pretty much any language in the world
- the way we combine those sounds: consonant + vowel + n (optional)
  some extra rules that are not too relevant

## Vocabulary

- originally: just 120 words!
- many words have their origins in other languages
  "toki pona":
  - toki -> tok -> talk
  - pona -> bonus -> bon
- most remarkable feature of the language: words are categories, concepts, ideas -> no direct english translations (ex. toki, pona)
- lack of specific vocabulary: combinations are used (subject, then adjectives that modify it like in French)
- example: tomo can mean "building", pali "to work"
  so what could "tomo pali" be then?
- but "work building" is obviously vague, and on top of that, these are not the only possible meanings of tomo and pali
- tomo pali could also mean "build room" = workshop?
- shows that paraphrasing and context are necessary 
  
## Grammar

- let's finish by showing a tiny bit of grammar
- toki pona uses a simple subject - verb - object structure for basic sentences
- parts of speech are separated by *particles* (li comes before verb, e before object)
- But: no articles, time, gender, plural/singular, cases, ... => No inflection for any of this (i.e. no changing words)
- this sentence means all of this at the same time...
- more grammar than just this - there are questions, exclamations, emphasis, usually all using a single word

## Summary
- there are tons of languages out there that are made to explore linguistic ideas (conlangs)
- toki pona is a philosophical experiment about simplicity
- tiny vocabulary, easy pronunciation and straight-forward grammar
- you should give it a try!

## Recommendations

- Want to learn? "Toki Pona - The Language of Good" by the creator, Sonja Lang (Can lend it to you if interested)
- or join the toki pona Discord server for many free resources
- jan Misali (YouTube) - Videos about conlangs (including toki pona) and other random stuff
- (Casual) Linguistics: K Klein, LingoLizard (YouTube)
