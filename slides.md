---
theme: default
class: text-center
info: |
  ## English Presentation on toki pona
drawings:
  persist: false
transition: slide-left
css: unocss
title: toki pona
---

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Toki_pona.svg/260px-Toki_pona.svg.png" class="m-auto" />

<v-click>

# toki pona

</v-click>


---
transition: default
layout: center
---


# How many vowels does French have?

---

<style>
li {
  @apply text-3xl;
}

</style>

<h1 class="mb-3">About 14.</h1>

<div class="grid grid-cols-2">
<div>
<ul>
  <li>/i/ <i>(si)</i></li>
  <li>/e/ <i>(fée)</i></li>
  <li>/ɛ/ <i>(fait)</i></li>
  <li>/y/ <i>(su)</i></li>
  <li>/ø/ <i>(ceux)</i></li>
  <li>/œ/ <i>(sœur)</i></li>
  <li>/ə/ <i>(ce)</i></li>
</ul>
</div>

<div>
<ul>
  <li>/u/ <i>(sous)</i></li>
  <li>/o/ <i>(sot)</i></li>
  <li>/ɔ/ <i>(sort)</i></li>
  <li>/a/ <i>(sa)</i></li>
  <li>/ɑ̃/ <i>(sans)</i></li>
  <li>/ɔ̃/ <i>(son)</i></li>
  <li>/ɛ̃/ <i>(brin)</i></li>
</ul>
</div>
</div> 

---
class: text-center
---

<h1><span class="text-5xl">Phonology</span>
<br>
<span class="text-gray-400 text-4xl">kalama</span></h1>

<v-click>

<h2 class="mt-70px text-blue-400 font-semibold">a e i o u</h2>
<h2 class="text-red-400 font-semibold">j k l m n p s t w</h2>

<br>

<h2>Syllable = <span class="text-red-400">Consonant</span>? + <span class="text-blue-400">Vowel</span> + (<span class="text-red-400">n</span>)</h2>

</v-click>

---
class: text-center
transition: default
---

<style>

.label-toki {
  position: absolute;
  @apply text-3xl text-amber-500;
}

.label-pona {
  position: absolute;
  @apply text-3xl text-lime-500;
}

</style>

<h1 class="text-center"><span class="text-5xl">Vocabulary</span>
<br>
<span class="text-gray-400 text-4xl">nimi</span></h1>


<h2 class="mt-100px"><span class="text-5xl">
<span class="text-amber-600 font-semibold">toki </span>
<span class="text-lime-600 font-semibold">pona</span>
</span></h2>

<div v-click>

<span class="label-toki top-155px left-195px">communication</span>
<span class="label-toki top-210px left-250px">thought</span>
<span class="label-toki top-265px left-215px">speech</span>
<span class="label-toki top-320px left-275px">language</span>

<span class="label-pona top-170px left-600px">good</span>
<span class="label-pona top-225px left-640px">simple</span>
<span class="label-pona top-280px left-620px">friendly</span>
<span class="label-pona top-335px left-570px">peaceful</span>

</div>

<!-- <v-click> -->

<!-- <span class="text-3xl"> -->
<!-- <span class="text-lime-600">good </span> -->
<!-- <span class="text-amber-600">language</span> -->
<!-- </span> -->

<!-- </v-click> -->


---
class: text-center
transition: default
---

<h1 class="text-center"><span class="text-5xl">Vocabulary</span>
<br>
<span class="text-gray-400 text-4xl">nimi</span></h1>

<div class="mt-80px flex basis-1/3 gap-8 place-items-center justify-center text-4xl">
<div>
tomo
<br>
<span text-2xl text-gray-700>(building)</span>
</div>
<div>+</div>
<div>
pali
<br>
<span text-2xl text-gray-700>(to work)</span>
</div>
</div>

<v-click>
<div text-4xl>
<div my-25px>⬇</div>

<div font-semibold>office</div>
</div>
</v-click>

---
class: text-center
---

<h1 class="text-center"><span class="text-5xl">Vocabulary</span>
<br>
<span class="text-gray-400 text-4xl">nimi</span></h1>

<div class="mt-80px flex gap-8 basis-1/3 place-items-center justify-center text-4xl">
<div>
tomo
<br>
<span text-2xl text-gray-700>(room)</span>
</div>
<div>+</div>
<div>
pali
<br>
<span text-2xl text-gray-700>(to build)</span>
</div>
</div>

<v-click>
<div text-4xl>
<div my-25px>⬇</div>

<div font-semibold>workshop</div>
</div>
</v-click>

---
class: text-center
---


<h1><span class="text-5xl">Grammar</span>
<br>
<span class="text-gray-400 text-4xl">lawa</span></h1>

<div class="text-4xl gap-7 flex justify-center font-semibold mt-75px">
<div class="text-blue-500">
jan
<br>
<span class="text-2xl">subject</span>
</div>
<div class="font-bold">li</div>
<div class="text-red-500">
moku
<br>
<span class="text-2xl">verb</span>
</div>
<div class="font-bold">e</div>
<div class="text-green-500">
telo
<br>
<span class="text-2xl">object</span>
</div>
</div>

<div v-click class="mt-20px">
<span class="text-5xl">=</span>
<div class="mt-20px flex gap-7 text-3xl place-items-center justify-center font-semibold">
<div class="text-blue-500">
A person
<br>
People
<br>
The person
</div>
<div class="text-red-500">
drinks
<br>
will drink
<br>
drank
</div>
<div class="text-green-500">
water
<br>
liquids
<br>
a beverage
</div>
</div>
</div>

---
layout: center
class: text-center
---

<h1><span class="text-5xl">Summary</span>
<br>
<span class="text-gray-400 text-4xl">sona suli</span></h1>

---
class: text-center
---

<style>
ul {
  @apply text-left;
  @apply text-2xl;
  @apply mb-16
}
</style>

<h1><span class="text-5xl">Further Resources</span>
<br>
<span class="text-gray-400 text-4xl">lipu sona mute</span></h1>

- These slides can be found on [toki-pona.is-a-lot-of.fun](https://toki-pona.is-a-lot-of.fun)
- [tokipona.org](https://tokipona.org)
- [jan Misali](https://www.youtube.com/@HBMmaster) - conlangs, toki pona, random other stuff
- [K Klein](https://www.youtube.com/@kklein), [LingoLizard](https://www.youtube.com/@LingoLizard) - casual linguistics (natural languages)


<h2><span class="text-3xl">Do you have questions?</span>
<br>
<span class="text-gray-400 text-2xl">sina wile sona e seme?</span></h2>
